Genome aligner scripts can be run from the command-line with the following command:

    python main.py
    
    
    
There are 5 optional parameters (options)

    1. Fasta input file (default to 'MT.fa')
    
    2. Fastq input file (default to 'test.fastq')
    
    3. Number of window candidates per read for extention (default to 5)
    
    4. Seed length (default to 20)
    
    5. Seed skip interval (default to 10)
    



Examples of using cmd parameters:

    python main.py fasta.fa fastq.fq
    
    python main.py fasta.fa fastq.fq 5
    
    python main.py fasta.fa fastq.fq 5 20 10
    



Advised usage of optional parameters is as following:

    1) No parameters, 
    
    2) Only paths
    
    3) paths + windows no
    
    4) paths + windows no + seed lenth and skip interval
    