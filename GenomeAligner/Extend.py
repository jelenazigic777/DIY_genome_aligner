from LocalAligner import localAlignment, tracebak
from ChooseWindows import get_window
from Globals import globals

# Exteding phase and writing to SAM-alike file
def extend_and_output(reads, selected_windows_per_read):
    sam = open('result.txt', 'w')
    for read_name, read_content in reads.items():
        max_score = 0
        same_win_cnt = 1
        for window_pos in selected_windows_per_read[read_name]:
            current_win = get_window(window_pos[0], window_pos[1], globals['genome'], globals['genome_compl'])
            matrix, score = localAlignment(current_win, read_content[0])
            if score >= max_score:
                if score == max_score:
                    same_win_cnt += 1
                else:
                    same_win_cnt = 1
                max_score = score
                max_matrix = matrix
                max_pos = window_pos[0]
                max_flag = window_pos[1]
                max_window_content = current_win

        _, transcript = tracebak(max_window_content, read_content[0], max_matrix)

        sam.write('Read name: ' + read_name + '\n')
        sam.write('Complement flag: ' + str(max_flag) + '\n')
        sam.write('Reference name: ' + globals['ref_name'] + '\n')
        sam.write('Position: ' + str(max_pos) + '\n')
        sam.write('Mapping Quality: ' + str(max_score) + '\n')
        sam.write('CIGAR string: ' + transcript + '\n')
        sam.write('Read sequence: ' + read_content[0] + '\n')
        sam.write('Sequence quality: ' + read_content[1] + '\n')
        sam.write('No. of same-score windows: ' + str(same_win_cnt) + '\n\n\n')
        print ("Writing to SAM " + read_name)
    sam.close()
