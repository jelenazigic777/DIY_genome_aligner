from Globals import globals

# hits is list of exact seed matches for one read
def choose_windows_for_read(hits: list, n: int) -> list:
    all_windows = []
    window_size = 3*globals['read_len']
    # globals['read_len'] was chosen for windows moving interval
    for pos in range(0, globals['genome_len'], globals['read_len']):
        hits_per_win = len(list(x for x in hits if pos < x < pos + window_size))
        all_windows.append((pos, hits_per_win))
        all_windows.sort(key=lambda x: x[1], reverse=True)  # sorts in place
    return  all_windows[:n]

def get_window (pos: int, compl: bool, genome: str, genome_comp: str) -> str:
    window_size = 3*globals['read_len']
    genome_len = globals['genome_len']
    return genome_comp[genome_len - pos - window_size : genome_len - pos] if compl else genome[pos : pos + window_size]

def select_top_n_windows(reads, reads_hits, reads_hits_comp):
    windows_no =  globals['windows_no']
    selected_windows_per_read = {}
    for read_name in reads.keys():
        selected_windows_0 = choose_windows_for_read(reads_hits[read_name], windows_no) 
        selected_windows_1 = choose_windows_for_read(reads_hits_comp[read_name], windows_no)
        
        selected_windows = []
        k = 0
        j = 0
        for _ in range(windows_no):
            if selected_windows_0[k][1] >= selected_windows_1[j][1]:
                selected_windows.append((selected_windows_0[k][0], 0))
                k+=1
            else:
                selected_windows.append((selected_windows_1[j][0], 1))
                j+=1
        selected_windows_per_read[read_name] = selected_windows
    return selected_windows_per_read