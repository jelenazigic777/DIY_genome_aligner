import argparse
from FQparser import parse_fastq, cut_seeds, complement_all_seeds
from StructConverter import all_read_hits
from ChooseWindows import select_top_n_windows
from Extend import extend_and_output
from Globals import init

# Managing arguments
parser = argparse.ArgumentParser()
parser.add_argument('--input_fasta', default = 'MT.fa', help='Fasta input file')
parser.add_argument('--input_fastq',default = 'test.fastq', help='Fastq input file')
parser.add_argument('--windows_no', default = 5, help='Number of window candidates for extention')
parser.add_argument('--seed_len', default = 20, help='Seed length')
parser.add_argument('--skip_interval', default = 20, help='Seed skip interval')
args = parser.parse_args()

# Initializing structures and parsing
reads = parse_fastq(args.input_fastq)
seeds = cut_seeds(reads, args.seed_len, args.skip_interval)
seeds_comp = complement_all_seeds(seeds)

# Passing some information to store it globally
init(args.input_fasta, args.windows_no, len(next(iter(reads.values()))[0]))

# Finding positions of all seed exact matches for every read
reads_hits = all_read_hits(seeds, args.seed_len)
reads_hits_comp = all_read_hits(seeds_comp, args.seed_len)

# Selecting top 'windows_no' windows for each read
selected_windows_per_read = select_top_n_windows(reads, reads_hits, reads_hits_comp)

# Exteding phase and writing to SAM-alike file.
extend_and_output(reads, selected_windows_per_read)
