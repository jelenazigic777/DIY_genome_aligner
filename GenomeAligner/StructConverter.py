from HashIndex import IndexHash
from Globals import globals

def all_read_hits(all_seeds: list, seed_len: int) -> dict:
    read_hits = {}
    hits = []
    indexHash = IndexHash(globals['genome'], seed_len)
    for i in range(1, len(all_seeds)):
        hits_for_seed = indexHash.query(all_seeds[i][1])
        hits+=hits_for_seed

        if (all_seeds[i][0] != all_seeds[i-1][0]) or (i==len(all_seeds)-1):
            hits.sort()
            read_name = all_seeds[i-1][0]
            read_hits[read_name] = list(hits)
            hits.clear()     
    return read_hits
