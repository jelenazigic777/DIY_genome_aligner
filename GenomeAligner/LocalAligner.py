import numpy

def  scoringMatrixscoring (a,b):
    if a==b: return 1 # match
    if a=='_' or b=='_': return -7 # indel
    return -4 # mismatch

def localAlignment(x,y):
    D = numpy.zeros((len(x)+1,len(y)+1), dtype=int)

    for i in range(1,len(x)+1):
        for j in range(1,len(y)+1):
            D[i,j]=max(0, 
                       D[i-1,j]  + scoringMatrixscoring(x[i-1], '_'),
                       D[i,j-1]  + scoringMatrixscoring('_',   y[j-1]), 
                       D[i-1,j-1] + scoringMatrixscoring(x[i-1],y[j-1]))
    
    # find the cell in the table which has maximum value       
    localMax = D.max()
    return D, localMax

def tracebak(x, y, V):
    maxValue=numpy.where(V==V.max())
    i=maxValue[0][0]
    j=maxValue[1][0]
    ax,ay,am, tr = '','','',''
    while (i>0 or j>0) and V[i,j]!=0:
        if i>0 and j>0:
            sigma = 1 if x[i-1]==y[j-1] else 0
            d=V[i-1,j-1] + scoringMatrixscoring(x[i-1],y[j-1]) # diagonal move
        if i>0: v=V[i-1,j] + scoringMatrixscoring(x[i-1],'_')  # vertical move
        if j>0: h=V[i,j-1] + scoringMatrixscoring('_',y[j-1])  # horizontal move
        
        # diagonal is the best
        if d>=v and d>=h:
            ax += x[i-1]
            ay += y[j-1]
            if sigma==1:
                tr+='M'
                am+='|'
            else:
                tr+='R'
                am+=' '
            i-=1
            j-=1
        # vertical is the best
        elif v>=h:
            ax+=x[i-1]
            ay+='_'
            tr+='I'
            am+=' '
            i-=1
        # horizontal is the best
        else:
            ay+=y[j-1]
            ax+='_'
            tr+='D'
            am+=' '
            j-=1
    alignment= '\n'.join([ax[::-1], am[::-1], ay[::-1]])
    return alignment, trace_to_cigar(tr[::-1]) 

def trace_to_cigar (trace: str) -> str:
    cigar = []
    cnt = 1
    for i in range(1, len(trace) + 1):
        if (i == len(trace) or trace[i]!=trace[i-1]):
            cigar += str(cnt)
            cigar += trace[i-1]
            cnt = 1
        else:
            cnt+=1
    return ''.join(cigar)
