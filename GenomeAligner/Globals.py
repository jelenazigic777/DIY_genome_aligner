from FQparser import complement
globals = {}

def init(fasta_path, windows_no, read_len):
    fasta = open(fasta_path)
    globals['windows_no'] = windows_no
    globals['ref_name'] = fasta.readline().rstrip()
    globals['genome'] = fasta.readline().rstrip()
    globals['genome_compl'] = complement(globals['genome'])
    globals['genome_len'] = len(globals['genome'])
    globals['read_len'] = read_len
    fasta.close()
