def parse_fastq(fastq_path: str) -> dict:
    my_file = open(fastq_path, 'r')
    all_lines = my_file.readlines()
    my_file.close()

    reads = {}
    for i in range(0, len(all_lines), 4):
        reads[all_lines[i].rstrip()] = (all_lines[i+1].rstrip(), all_lines[i+3].rstrip())
    return reads

    # Cutting each read to multiple seeds
def cut_seeds(reads: dict, seed_len: int, skip_interval: int) -> list:
    all_seeds = []
    for read_name, read_value in reads.items():
        all_seeds += read_to_seeds(read_name, read_value[0], seed_len, skip_interval)

    return all_seeds

def read_to_seeds(read_name: str, read_content: str, seed_len: int, skip_interval: int) -> list:
    seed_list = []
    
    for i in range(0, len(read_content) - skip_interval, skip_interval):
        seed_list.append(tuple([read_name, read_content[i:i+seed_len], '0']))
    
    return seed_list

def complement_all_seeds(seed_list: list) -> list:
    return list(map(lambda entry: tuple([entry[0], complement(entry[1]), '1']), seed_list))

def complement(content: str) -> str:
    return (content.replace('A', '@').replace('T', '#').replace('@', 'T').replace('#', 'A')
    .replace('C', '@').replace('G', '#').replace('@', 'G').replace('#', 'C'))[::-1]
